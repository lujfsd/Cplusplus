/**
 * @file alignas.cpp
 * @author lujiefeng
 * @brief 
 * @version 0.1
 * @date 2020-06-03
 * 
 * @copyright Copyright (c) 2020
 * 
 * g++ -o alignas alignas.cpp -std=c++17
 */

#include <iostream>

void testBase()
{
    std::cout << "alignof(char):" << alignof(char) << std::endl
              << "alignof(short):" << alignof(short) << std::endl
              << "alignof(int):" << alignof(int) << std::endl
              << "alignof(float):" << alignof(float) << std::endl
              << "alignof(double):" << alignof(double) << std::endl
              << "alignof(long):" << alignof(long) << std::endl;
}

void testStruct()
{
    struct testStr
    {
        char data;
        int len;
    } testData;
    std::cout << "alignof(struct):" << alignof(struct testStr) << std::endl;
}

void testPackOne()
{
#pragma pack(1)
    struct testPackOne
    {
        char data;
        int len;
    } testPackOneData;
    std::cout << "alignof(struct testPackOne):" << alignof(struct testPackOne) << std::endl;
#pragma pack()

    /*
    *编译器报错: error: requested alignment is less than minimum alignment of 4 for type *'testAlignas'
    *    struct alignas(1) testAlignas 
    *    {
    *    char data;
    *    int len;
    *    } testAlignasData;  
    */
    struct alignas(8) testAlignas
    {
        char data;
        int len;
    } testAlignasData;
    std::cout << "alignof(struct testAlignas):" << alignof(struct testAlignas) << std::endl;
}

int main()
{
    testBase();
    testStruct();
    testPackOne();
    return 0;
}