
# 概念

alignas：对齐要求即字节对齐，指定某个类型是几字节对齐。
alignof：查询类型的对齐要求

# 字节对齐

内存中是按照字节来划分存储空间的，理论上讲数据存储在内存上时，也会是按照字节顺序进行排列，但是CPU在访问内存数据时，为了提高效率通常会按照自身位数对数据进行存取。如32位的CPU，访问内存是会一次性获取到32bit数据，如果一个32位的数据没有放在被4字节整除的地址处时，此时CPU需要读取两次才可以获取到该数据，显然是效率打折了。
但是有时在嵌入式开发过程中，为了节省资源，方便处理字节流的数据，往往会采用一字节对齐的方式。



# 对齐准则

1. 数据类型自身的对齐值：

| 类型   | 对齐值 |
| ------ | ------ |
| char   | 1      |
| short  | 2      |
| int    | 4      |
| float  | 4      |
| long   | 8      |
| double | 8      |

2. 结构体或者类的自身对齐值：其成员中自身对齐值最大的那个值。

```
    struct testStr
    {
        char data;
        int len;
    } ;
   std::cout << "alignof(struct):" << alignof(struct testStr) << std::endl;
```

结构体testStr的对齐值是4，按照int类型进行对齐。

3. 指定对齐值：#pragma pack (value)时或者alignas(value)指定对齐值value。

```
#pragma pack(1)
    struct testPackOne
    {
        char data;
        int len;
    } testPackOneData;
    std::cout << "alignof(struct):" << alignof(struct testPackOne) << std::endl;
#pragma pack()

    struct alignas(8) testAlignas
    {
        char data;
        int len;
    } testAlignasData;
    std::cout << "alignof(struct testAlignas):" << alignof(struct testAlignas) << std::endl;
```

结构体testPackOne对齐值是1，按照指定的value对齐。
结构体PackOne对齐值是8，按照指定的value对齐，⚠️注意：alignas对齐要求必须大于等于该类型内最大的对齐值。修改为以下代码时编译器保存：

```
	struct alignas(1) testAlignas
    {
        char data;
        int len;
    } testAlignasData;
    std::cout << "alignof(struct testAlignas):" << alignof(struct testAlignas) << std::endl;
```

   错误信息：

  ```
alignas.cpp:46:12: error: requested alignment is less than minimum alignment of 4 for type 'testAlignas'
    struct alignas(1) testAlignas
           ^
1 error generated.
  ```

4.	数据成员、结构体和类的有效对齐值：自身对齐值和指定对齐值中小的那个值。
